from django.contrib import admin
from .models import Receipt, ExpenseCategory, Account

# Register your models here.
admin.site.register(Receipt)
admin.site.register(Account)
admin.site.register(ExpenseCategory)
