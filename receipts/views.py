from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import CreateRecipeForm, CreateCategoryForm
from .forms import CreateAccountForm

# Create your views here.
@login_required
def display_receipts(request):
    list_of_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list_of_receipts": list_of_receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_new_receipt(request):
    if request.method == "POST":
        form = CreateRecipeForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.purchaser = request.user
            form.save()

            return redirect("home")
    else:
        form = CreateRecipeForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def display_expense_category(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_categories": expense_categories
    }
    return render(request, "receipts/categories.html", context)


@login_required
def display_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.owner = request.user
            form.save()

            return redirect("list_categories")
    else:
        form = CreateCategoryForm()

    context = {
        "form": form
    }
    return render(request, "receipts/createcat.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.owner = request.user
            form.save()

            return redirect("accounts")
    else:
        form = CreateAccountForm()

    context = {
        "form": form
    }
    return render(request, "receipts/createacc.html", context)
