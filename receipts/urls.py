from .views import display_receipts, create_new_receipt
from .views import display_expense_category, display_accounts
from .views import create_category, create_account
from django.urls import path

urlpatterns = [
    path("", display_receipts, name="home"),
    path("create/", create_new_receipt, name="create_receipt"),
    path("categories/", display_expense_category, name="list_categories"),
    path("accounts/", display_accounts, name="accounts"),
    path("categories/create/", create_category, name="create_cat"),
    path("accounts/create/", create_account, name="create_account")
]
