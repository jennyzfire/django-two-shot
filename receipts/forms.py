from django import forms
from .models import Receipt, ExpenseCategory, Account


class CreateRecipeForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "category",
            "account",
        ]


class CreateCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
